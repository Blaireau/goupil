# Goupil

A simple logic game made with haskell

![](goupil.png)

## Rules

Move the fox to eat all the rabbits !
Every time you move, all the rabbits move along, in the same direction.
Be careful, if a rabbit reaches one of your castles, you loose !

## Controls

Use the arrow keys to move.
Press `q` to quit the game.

## Usage

Run the game in a terminal.

You can set a different seed to change the level.
You can also set a different grid size.

```
goupil <seed> <width>x<height>
```

You will need a terminal with an emoji font installed to run the game.
