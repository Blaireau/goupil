{-# LANGUAGE OverloadedStrings #-}

{-
    Goupil, a simple logic game made with haskell
    Copyright (C) 2023  Blaireau

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}


import Data.Array.IArray
import System.IO
import System.Environment
import Text.Read (readMaybe)
import qualified Data.Text as T
import qualified Data.Text.IO as T

-------------------------------------------------------------------------------
-- GAME LOGIC
-------------------------------------------------------------------------------

class GameState a where
    makeMove :: a -> String -> a
    isVictory :: a -> Maybe Bool

data GroundType = Grass | Tree | Border | Castle | Fox | Rabbit deriving Eq

type TerrainSize = ((Int,Int), Position)

data Terrain = Terrain {
    cells :: Array Position GroundType
}

type Position = (Int, Int)

data TDGame = TDGame {
    turn :: Integer,
    board :: Terrain,
    player :: Position,
    rabbits :: [Position]
}

getDelta :: String -> Position
getDelta "left" = (-1, 0)
getDelta "right" = (1, 0)
getDelta "up" = (0, -1)
getDelta "down" = (0, 1)
getDelta _ = (0, 0)

rotateDelta :: Position -> Position
rotateDelta (x,y) = if x == 0 then (y,x) else (-y,-x)

moveEntity :: Terrain -> Position -> Position -> Position
moveEntity terrain delta entity = if validMove then newEntity else entity
    where
        (x,y) = entity
        (dx,dy) = delta
        newEntity = (x+dx,y+dy)
        validMove = freeCell terrain newEntity

instance GameState TDGame where
    makeMove board@(TDGame turn terrain player rabbits) move = newGame where
        delta = getDelta move
        rdelta = delta
        newPlayer = moveEntity terrain delta player
        newRabbits = filter (/= newPlayer) $ map (moveEntity terrain rdelta) rabbits
        newTurn = turn + 1
        newGame = if newPlayer /= player
            then TDGame newTurn terrain newPlayer newRabbits
            else board

    isVictory board@(TDGame turn terrain player rabbits)
        | rabbits == [] = Just True
        | Castle `elem` map (\r -> (cells terrain)!r) rabbits = Just False
        | otherwise = Nothing

freeCell :: Terrain -> Position -> Bool
freeCell (Terrain cells) pos@(x,y) = inBounds && canWalkOnCell
    where
        ((xl,yl), (xh,yh)) = bounds cells
        inBounds = xl <= x && x <= xh && yl <= y && y <= yh 
        cellType = cells!pos
        canWalkOnCell = cellType == Grass || cellType == Castle

quasiRand :: Int -> Int -> Int
quasiRand n m = ((33+n*m)*4697) `mod` 100

makeCell :: Int -> TerrainSize -> (Int,Int) -> GroundType
makeCell seed ((xl,yl), (xh,yh)) (x,y)
    | onBorder = Border
    | onCentralCell || onCorner = Castle
    | quasiRand (seed*x^2+11*y) (seed*y^2+13*x) < 10 = Tree
    | otherwise = Grass
    where
        onBorder = x == xl || x == xh || y == yl || y == yh
        onCorner = (x == xl+1 || x == xh-1) && (y == yl+1 || y == yh-1)
        onCentralCell =    (xh - xl) `div` 2 + xl == x
                        && (yh - yl) `div` 2 + yl == y

makeTerrain :: Int -> TerrainSize -> Terrain
makeTerrain seed terrainSize@((xl,yl), (xh,yh)) = Terrain 
    $ array terrainSize [
        ((x,y), makeCell seed terrainSize (x,y)) | x <- [xl..xh], y <- [yl..yh]
    ] 

makeNewGame :: Int -> Position -> TDGame
makeNewGame seed size = TDGame 1 terrain start rabbits
    where
        terrainSize = ((0,0), size) :: TerrainSize
        terrain = makeTerrain seed terrainSize
        (w,h) = size
        randPos = \s -> (1 + (quasiRand 5 s) `mod` (w-2), 
                         1 + (quasiRand 23 s) `mod` (h-2))
        start = randPos seed
        rabbitsCount = (w*h) `div` 20
        rabbits = filter (\r -> cells terrain ! r == Grass) [randPos (2*seed+i+5) | i <- [1..rabbitsCount]]

-------------------------------------------------------------------------------
-- DISPLAY LOGIC
-------------------------------------------------------------------------------

class Displayable a where
    display :: a -> T.Text

type Entity = T.Text

instance Displayable GroundType where
    display Grass = "  "
    display Tree = "🌳"
    display Border = "🌲"
    display Castle = "🏰"
    display Fox = "🦊"
    display Rabbit = "🐰"

instance Displayable Terrain where
    display (Terrain cells) = T.intercalate "\n" [
                              T.intercalate "" [
                              display (cells!(i,j)) 
                              | i <- [si..ei]] 
                              | j <- [sj..ej]] where
         ((si, sj), (ei, ej)) = bounds cells

instance Displayable TDGame where
    display board@(TDGame turn terrain player rabbits) = T.append 
        (T.pack $ "Turn: "++show turn++" | Remaining rabbits: "++show rabbitsCount++"\n")
        (display $ Terrain newCells)
        where 
            newCells = cells // entitiesDisp
            entitiesDisp = (player, Fox):rabbitsDisp
            rabbitsDisp = map (\p -> (p, Rabbit)) rabbits
            rabbitsCount = length rabbits
            Terrain cells = terrain


-------------------------------------------------------------------------------
-- CONTROLER LOGIC
-------------------------------------------------------------------------------

aliases :: String -> String
aliases "t" = "left"
aliases "r" = "right"
aliases "d" = "up"
aliases "s" = "down"
aliases "\ESC[D" = "left"
aliases "\ESC[C" = "right"
aliases "\ESC[A" = "up"
aliases "\ESC[B" = "down"
aliases "q" = "quit"
aliases s = s

cleanScreen = "\ESC[2J"
cursorHome = "\ESC[H" 
hideCursor = "\ESC[?25l"
showCursor = "\ESC[?25h"

clearWindow :: IO ()
clearWindow = Prelude.putStr $ cleanScreen ++ cursorHome

readParameters :: IO (Maybe (Int, Position))
readParameters = do
    args <- getArgs
    case args of
        [] -> return $ Just (1, (10,10))
        [seed] -> case readMaybe seed :: Maybe Int of
            Just n -> return $ Just (n, (10, 10))
            Nothing -> return Nothing
        [seed, size] -> return output where
            m_seed = readMaybe seed :: Maybe Int
            m_size = map (\t -> readMaybe $ T.unpack t :: Maybe Int) (T.splitOn "x" (T.pack size))
            output = case (m_seed, m_size) of
                (Just n, [Just w, Just h]) -> Just (n, (w,h))
                _ -> Nothing
    

getKey :: IO [Char]
getKey = reverse <$> getKey' ""
  where getKey' chars = do
          char <- getChar
          more <- hReady stdin
          (if more then getKey' else return) (char:chars)

drawGame :: Displayable a => a -> IO ()
drawGame game = clearWindow >> (T.putStrLn $ display game)


play :: Displayable a => GameState a => a -> IO ()
play game = do
    drawGame game
    rawMove <- getKey
    let move = aliases rawMove
    let newGame = makeMove game move
    let victory = isVictory newGame
    drawGame newGame
    if move == "quit" then 
        return () 
    else case victory of
        Just True -> putStrLn "Victory !"
        Just False -> putStrLn "You loose !"
        Nothing -> play newGame
    
main = do
    params <- readParameters
    if params == Nothing then do
        putStrLn "Invalid parameters.\n"
        putStrLn "Usage: goupil [<seed> [<width>x<height>]]\n"
        putStrLn "Example:"
        putStrLn "    goupil 3 24x18\n        play the game with seed 3 on a 24 by 18 grid"
    else do
        let Just (seed, size) = params
        -- setup some options for a better TUI
        hSetBuffering stdout NoBuffering
        hSetBuffering stdin NoBuffering
        hSetEcho stdin False
        Prelude.putStr $ cleanScreen ++ cursorHome ++ hideCursor
        -- launch the game
        let game = makeNewGame seed size
        play game
        -- reset terminal to a correct state
        Prelude.putStrLn $ showCursor
